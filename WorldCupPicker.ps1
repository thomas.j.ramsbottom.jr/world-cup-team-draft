﻿##World Cup Team Draft##

## Fifa List ##
# https://www.fifa.com/fifaplus/en/articles/qatar-2022-all-qualified-teams-groups-dates-match-schedule-tickets-more

## Vars ##
$List = Get-Content -Path ./UserList.txt
$Prizes = Get-Content -Path ./country.txt
$Participants = [System.Collections.ArrayList] ($List)

Write-Host "
WELCOME TO YOUR TEAM DRAFT

 o__        o__     |   |\
/|          /\      |   |X\
/ > o        <\     |   |XX\

    World Cup 2022!
"
Write-Host "
########## How it Works ##########
This script will take a random
person from a list and place it with
a team. The teams will be listed
from their groups. The sciprt will
start from the top and work its way
down. The order is as listed from:
https://www.fifa.com/
Once a team and person is selected
they will be removed from the list.
"
Write-Host ""
Write-Host "
########## PRIZES ##########
1st: 500
2nd: 100
3rd: 40
"

Read-Host "Press ENTER to start"

Write-Host ""
Write-Host "
GOOD LUCK TO EVERYONE!
"
sleep 3
clear

foreach ($Prize in $Prizes) {
    $Participant = Get-Random -InputObject $Participants
    Write-Host "
########## World Cup 2022 ##########

          $Prize

####################################"
    sleep 3
    Write-Host ""
    Write-Host "The winner is ......"
    sleep 3
    Write-Host""
    Write-Host "$Participant" -ForegroundColor Green
    Write-Host ""
    Write-Host "Congrats to $Participant on $Prize"
    Write-Host ""
    echo "$Participant; $Prize" >> WordlCupTeamsList.txt
    $Participants.Remove($Participant)
    sleep 5
    clear
    }

Write-Host "
 o__        o__     |   |\
/|          /\      |   |X\
/ > o        <\     |   |XX\

    World Cup 2022!
"
sleep 5
start 
